#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "Task.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    // MAINWINDOW_H

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void updateStatus();

public slots:
    void addTask();
    void removeTask(Task* task);
    void taskStatusChanged(Task* task);
    void changeStyle();
    void changeFont();

private:
    Ui::MainWindow *ui;
    QVector<Task*> mTasks;    
    QString fileName = ":dark.qss";
};
#endif
