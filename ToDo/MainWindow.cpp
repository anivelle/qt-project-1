#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QInputDialog>
#include <QFile>
#include <QTextStream>
#include <QFontDatabase>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      mTasks()
{
    ui->setupUi(this);
    connect(ui->addTaskButton, &QPushButton::clicked,
            this, &MainWindow::addTask);
    connect(ui->switchStyleButton, &QPushButton::clicked,
            this, &MainWindow::changeStyle);
    QFile file(":/light.qss");
        file.open(QFile::ReadOnly | QFile::Text);
        QTextStream stream(&file);
        setStyleSheet(stream.readAll());
    updateStatus();
    changeFont();
    ui->taskName->selectAll();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTask()
{
    QString name = ui->taskName->text();

    if (!name.isEmpty())
    {
        qDebug() << "Adding new task";
        Task* task = new Task(name);
        connect(task, &Task::removed,
                this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged,this,
                &MainWindow::taskStatusChanged);
        mTasks.append(task);
        ui->tasksLayout->addWidget(task);
        updateStatus();
    }
    ui->taskName->setText("New Task");
    ui->taskName->selectAll();
}

void MainWindow::removeTask(Task* task)
{
    mTasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    delete task;
    updateStatus();
}

void MainWindow::taskStatusChanged(Task* /*task*/)
{
    updateStatus();
}

void MainWindow::updateStatus()
{
    int completedCount = 0;
    for (auto t : mTasks)
    {
        if (t -> isCompleted())
        {
            completedCount++;
        }
    }
    int todoCount = mTasks.size() - completedCount;

    ui->statusLabel->setText(

        QString("Status: %1 todo / %2 completed")
                            .arg(todoCount)
                            .arg(completedCount));
}

void MainWindow::changeStyle()
{
    QString buttonText;
    QFile file(fileName);
    file.open(QFile::ReadOnly | QFile::Text);
    QTextStream stream(&file);
    setStyleSheet(stream.readAll());
    if (fileName == ":dark.qss")
    {
        fileName = ":light.qss";
        buttonText = "\uf185";
    } else
    {
        fileName = ":dark.qss";
        buttonText = "\uf186";
    }
    ui->switchStyleButton->setText(buttonText);
}

void MainWindow::changeFont()
{
    QFontDatabase::addApplicationFont(":/FontAwesome.otf");

    QFont font;
    font.setFamily("FontAwesome");
    font.setPixelSize(20);

    ui->addTaskButton->setFont(font);
    ui->switchStyleButton->setFont(font);

    ui->addTaskButton->setText("\uf067");
    ui->switchStyleButton->setText("\uf186");
}
